const internal = {
  timeout: 60
}

const api = {
  url: "http://internal.cubo.mobi:8001/ords/skyvision",
  version_request: "/v1/",
}

const ords = {
  module_avgunit : "report/unit/avgcritical"
}

module.exports = cfg = {
  api, internal, ords
}